package ru.kozyrev.tm.entity;


public class Task {
    private String name;
    private int id;
    private int projectId;

    public Task(String name, int projectId, int id) {
        this.name = name;
        this.id = id;
        this.projectId = projectId;
    }

    public int getProjectId() {
        return projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }
}