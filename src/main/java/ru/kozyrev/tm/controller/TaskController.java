package ru.kozyrev.tm.controller;

import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.Task;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class TaskController {
    private List<Task> listTasks = new ArrayList<>();
    private ProjectController projectController;
    private Scanner sc;
    private int taskId;

    public TaskController(Scanner sc) {
        this.sc = sc;
        taskId = 1;
        //Демо данные
        listTasks.add(new Task("Пункт 1", 1, taskId++));
        listTasks.add(new Task("Пункт 2", 1, taskId++));
        listTasks.add(new Task("Пункт 3", 1, taskId++));
        listTasks.add(new Task("Пункт 4", 1, taskId++));
        listTasks.add(new Task("Пункт 5", 1, taskId++));
        listTasks.add(new Task("Пункт 1", 2, taskId++));
        listTasks.add(new Task("Пункт 2", 2, taskId++));
        listTasks.add(new Task("Пункт 3", 2, taskId++));
        listTasks.add(new Task("Пункт 1", 3, taskId++));
        listTasks.add(new Task("Пункт 2", 3, taskId++));
        listTasks.add(new Task("Пункт 3", 3, taskId++));
        listTasks.add(new Task("Пункт 4", 3, taskId++));
        listTasks.add(new Task("Пункт 5", 3, taskId++));
        listTasks.add(new Task("Пункт 6", 3, taskId++));
    }

    public void setProjectController(ProjectController projectController) {
        this.projectController = projectController;
    }

    public void create() {
        System.out.println("[TASK CREATE]\nENTER NAME:");
        String name = sc.nextLine();
        if (name.length() == 0) {
            System.out.println("Name is empty!");
        } else {
            System.out.println("ENTER PROJECT ID:");
            try {
                int projectId = Integer.parseInt(sc.nextLine());
                Project project = projectController.getProjectById(projectId);
                if (project != null) {
                    listTasks.add(new Task(name, projectId, taskId));
                    printList(projectId);
                } else {
                    throw new IndexOutOfBoundsException();
                }
            } catch (NumberFormatException | IndexOutOfBoundsException e) {
                System.out.println("Wrong index!");
            }
        }
    }

    public void update() {
        System.out.println("[UPDATE TASK]\nENTER ID:");
        try {
            Task task = getTaskById(Integer.parseInt(sc.nextLine()));
            if (task != null) {
                System.out.println("NEW NAME:" + " (default: " + task.getName() + ")");
                String name = sc.nextLine();
                if (name.length() != 0) {
                    task.setName(name);
                }
                System.out.println("NEW PROJECT ID:" + " (default: " + task.getProjectId() + ")");
                String id = sc.nextLine();
                if (id.length() != 0) {
                    setProjectId(task, Integer.parseInt(id));
                }
                printList(Integer.parseInt(id));
            }
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            System.out.println("Wrong index!");
        }
    }

    public void getList() {
        System.out.println("PRESS 'ENTER' FOR ALL OR INPUT ID PROJECT");
        String projectId;
        if ((projectId = sc.nextLine()).length() == 0) {
            printList();
        } else {
            try {
                printList(Integer.parseInt(projectId));
            } catch (NumberFormatException e) {
                System.out.println("Wrong index!");
            }
        }
    }

    public void printList() {
        System.out.println("[TASKS LIST]");
        for (int i = 0; i < listTasks.size(); i++) {
            System.out.printf("%d. %s, ProjID=%d, ID#%d\n", i + 1, listTasks.get(i).getName(), listTasks.get(i).getProjectId(), listTasks.get(i).getId());
        }
    }

    public void printList(int projectId) {
        if (projectController.getProjectById(projectId) != null) {
            System.out.printf("[TASKS LIST OF PROJECT '%d']\n", projectId);
            int count = 1;
            for (int i = 0; i < listTasks.size(); i++) {
                if (listTasks.get(i).getProjectId() == projectId) {
                    System.out.printf("%d. %s, ID#%d\n", count++, listTasks.get(i).getName(), listTasks.get(i).getId());
                }
            }
        } else {
            System.out.println("Wrong index!");
        }
    }

    public void remove() {
        System.out.println("[TASK DELETE]\nENTER ID:");
        try {
            int taskId = Integer.parseInt(sc.nextLine());
            int projectId = getTaskById(taskId).getProjectId();
            listTasks.remove(getTaskById(taskId));
            printList(projectId);
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            System.out.println("Wrong index!");
        }
    }

    public void clear() {
        System.out.println("[CLEAR]");
        System.out.println("PRESS 'ENTER' FOR ALL OR INPUT ID PROJECT]");
        String id;
        if ((id = sc.nextLine()).length() == 0) {
            clearAll();
        } else {
            try {
                clearProjectTasks(Integer.parseInt(id));
            } catch (NumberFormatException e) {
                System.out.println("Wrong index!");
            }
        }
    }

    public void clearAll() {
        listTasks.clear();
        System.out.println("[ALL TASKS REMOVED]");
    }

    public void clearProjectTasks(int projectId) {
        Iterator<Task> iterator = listTasks.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getProjectId() == projectId) {
                iterator.remove();
            }
        }
        System.out.printf("[TASKS OF PROJECT ID=%d JUST REMOVED]\n", projectId);
    }

    public void setProjectId(Task task, int projectId) {
        if (projectController.getProjectById(projectId) != null) {
            task.setProjectId(projectId);
        }
        System.out.println("Wrong index!");
    }

    public Task getTaskById(int id) {
        for (int i = 0; i < listTasks.size(); i++) {
            if (listTasks.get(i).getId() == id) {
                return listTasks.get(i);
            }
        }
        return null;
    }
}
