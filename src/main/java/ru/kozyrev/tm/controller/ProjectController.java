package ru.kozyrev.tm.controller;

import ru.kozyrev.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ProjectController {
    private List<Project> listProjects = new ArrayList<>();
    private TaskController taskController;
    private Scanner sc;
    private int projectId;

    public ProjectController(Scanner sc) {
        this.sc = sc;
        projectId = 1;
        //Демоданные
        listProjects.add(new Project("Проект 1", projectId++));
        listProjects.add(new Project("Проект 2", projectId++));
        listProjects.add(new Project("Проект 3", projectId++));
        listProjects.add(new Project("Проект 4", projectId++));
        listProjects.add(new Project("Проект 5", projectId++));
        listProjects.add(new Project("Проект 6", projectId++));
    }

    public void setTaskController(TaskController taskController) {
        this.taskController = taskController;
    }

    public void create() {
        System.out.println("[PROJECT CREATE]\nENTER NAME:");
        String name = sc.nextLine();
        if (name.length() != 0) {
            listProjects.add(new Project(name, projectId++));
            printList();
        } else {
            System.out.println("Name is empty!");
        }
    }

    public void remove() {
        try {
            System.out.println("[PROJECT REMOVE]\nENTER ID:");
            int projectId = Integer.parseInt(sc.nextLine());
            if (getProjectById(projectId) != null) {
                listProjects.remove(getProjectById(projectId));
                System.out.printf("[PROJECT ID=%d REMOVED]\n", projectId);
                taskController.clearProjectTasks(projectId);
                printList();
            } else {
                throw new IndexOutOfBoundsException();
            }
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            System.out.println("Wrong index!");
        }
    }

    public void update() {
        System.out.println("[UPDATE PROJECT]\nENTER ID:");
        try {
            Project project = getProjectById(Integer.parseInt(sc.nextLine()));
            if (project != null) {
                System.out.println("[UPDATE PROJECT]\nENTER NEW NAME:");
                String name = sc.nextLine();
                if (name.length() != 0) {
                    project.setName(name);
                    printList();
                } else {
                    System.out.println("Name is empty!");
                }
            } else {
                throw new IndexOutOfBoundsException();
            }
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            System.out.println("Wrong index!");
        }
    }

    public void getList() {
        System.out.println("Press 'ENTER' for printing all projects list or input 'ID PROJECT'");
        try {
            String projectId = sc.nextLine();
            if (projectId.length() == 0) {
                printList();
            } else {
                printList(Integer.parseInt(projectId));
            }
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            System.out.println("Wrong index!");
        }
    }

    public void printList() {
        System.out.println("[PROJECTS LIST]");
        for (int i = 0; i < listProjects.size(); i++) {
            System.out.printf("%d. %s, ID#%d\n", i + 1, listProjects.get(i).getName(), listProjects.get(i).getId());
        }
    }

    public void printList(int projectId) {
        taskController.printList(projectId);
    }

    public void clear() {
        listProjects.clear();
        System.out.println("[CLEARED]");
    }

    public Project getProjectById(int projectId) {
        for (int i = 0; i < listProjects.size(); i++) {
            if (listProjects.get(i).getId() == projectId) {
                return listProjects.get(i);
            }
        }
        return null;
    }
}
