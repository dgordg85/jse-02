package ru.kozyrev.tm.manager;

import ru.kozyrev.tm.controller.ProjectController;
import ru.kozyrev.tm.controller.TaskController;
import ru.kozyrev.tm.enumerated.TerminalCommand;


import java.util.Scanner;

public class CommandManager {
    private ProjectController projectController;
    private TaskController taskController;

    public CommandManager(Scanner sc) {
        projectController = new ProjectController(sc);
        taskController = new TaskController(sc);
        projectController.setTaskController(taskController);
        taskController.setProjectController(projectController);
    }

    public void make(TerminalCommand command) {
        switch (command) {
            case HELP:
                TerminalCommand.printAlCommands();
                break;
            case PROJECT_CLEAR:
                projectController.clear();
                break;
            case PROJECT_UPDATE:
                projectController.update();
                break;
            case PROJECT_CREATE:
                projectController.create();
                break;
            case PROJECT_LIST:
                projectController.getList();
                break;
            case PROJECT_REMOVE:
                projectController.remove();
                break;
            case TASK_CLEAR:
                taskController.clear();
                break;
            case TASK_UPDATE:
                taskController.update();
                break;
            case TASK_CREATE:
                taskController.create();
                break;
            case TASK_LIST:
                taskController.getList();
                break;
            case TASK_REMOVE:
                taskController.remove();
                break;
        }
    }
}
